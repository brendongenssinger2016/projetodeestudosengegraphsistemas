﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.Sessao;
using ProjetoDeEstudo.Utilitarios.Integracao;

namespace ProjetoEstudos.Servidor.Interfaces
{
    [ServiceContract]
    public interface XISelos : IDisposable
    {
        [OperationContract]
        XUsuario Login(string pLogin, string pSenha);
    }
}
