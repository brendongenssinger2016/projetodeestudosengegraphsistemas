﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Engegraph.Comum.Utilitarios.PropertyChanged;


namespace ProjetoDeEstudo.Utilitarios.Integracao
{   
    [DataContract(Namespace = "http://engegraph.com.br/2013/05/Selodigital")]
    public class XUsuario : PropertyChangedBase
    {
        private int _chaveUsuario;
        private int _chavePerfil;
        private string _nome;
        private string _email;
        private string _login;
        private string _senha;
        private bool _master;
        private string _cargo;
        private bool _assinaDocumento;
        private bool _ativo;
        private DateTime? _ultimoLogin;
        private DateTime? _dataCriacao;
        private DateTime? _dataAtualizacao;

        [DataMember]
        public int ChaveUsuario
        {
            get { return _chaveUsuario; }
            set
            {
                if (value == _chaveUsuario) return;
                _chaveUsuario = value;
                NotifyOfPropertyChange(() => ChaveUsuario);
            }
        }

        [DataMember]
        public int ChavePerfil
        {
            get { return _chavePerfil; }
            set
            {
                if (value == _chavePerfil) return;
                _chavePerfil = value;
                NotifyOfPropertyChange(() => ChavePerfil);
            }
        }

        [DataMember]
        public string Nome
        {
            get { return _nome; }
            set
            {
                if (value == _nome) return;
                _nome = value;
                NotifyOfPropertyChange(() => Nome);
            }
        }

        [DataMember]
        public string Email
        {
            get { return _email; }
            set
            {
                if (value == _email) return;
                _email = value;
                NotifyOfPropertyChange(() => Email);
            }
        }

        [DataMember]
        public string Login
        {
            get { return _login; }
            set
            {
                if (value == _login) return;
                _login = value;
                NotifyOfPropertyChange(() => Login);
            }
        }

        [DataMember]
        public string Senha
        {
            get { return _senha; }
            set
            {
                if (value == _senha) return;
                _senha = value;
                NotifyOfPropertyChange(() => Senha);
            }
        }

        [DataMember]
        public bool Master
        {
            get { return _master; }
            set
            {
                if (value == _master) return;
                _master = value;
                NotifyOfPropertyChange(() => Master);
            }
        }

        [DataMember]
        public string Cargo
        {
            get { return _cargo; }
            set
            {
                if (value == _cargo) return;
                _cargo = value;
                NotifyOfPropertyChange(() => Cargo);
            }
        }

        [DataMember]
        public bool AssinaDocumento
        {
            get { return _assinaDocumento; }
            set
            {
                if (value == _assinaDocumento) return;
                _assinaDocumento = value;
                NotifyOfPropertyChange(() => AssinaDocumento);
            }
        }

        [DataMember]
        public bool Ativo
        {
            get { return _ativo; }
            set
            {
                if (value == _ativo) return;
                _ativo = value;
                NotifyOfPropertyChange(() => Ativo);
            }
        }

        [DataMember]
        public DateTime? UltimoLogin
        {
            get { return _ultimoLogin; }
            set
            {
                if (value.Equals(_ultimoLogin)) return;
                _ultimoLogin = value;
                NotifyOfPropertyChange(() => UltimoLogin);
            }
        }

        [DataMember]
        public DateTime? DataCriacao
        {
            get { return _dataCriacao; }
            set
            {
                if (value.Equals(_dataCriacao)) return;
                _dataCriacao = value;
                NotifyOfPropertyChange(() => DataCriacao);
            }
        }

        [DataMember]
        public DateTime? DataAtualizacao
        {
            get { return _dataAtualizacao; }
            set
            {
                if (value.Equals(_dataAtualizacao)) return;
                _dataAtualizacao = value;
                NotifyOfPropertyChange(() => DataAtualizacao);
            }
        }
    }
}
