﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.Configuracao;
using Engegraph.Comum.Utilitarios.Inicializador;

namespace Inicializador
{
   public class Program : XBaseDeInicializacao<XIPontoDeInicializacao>
    {

        public XConfig _config = new XConfig();

        public override string NomeProjeto => "Projeto de Estudo Engegraph Sistemas";
        public override string EnderecoVerificarVersao { get; }
        public override string EnderecoDownloadCliente { get; }
        public override string[] ArquivosNecessarios { get; }
        public override string NomeDllCliente => "ProjetoDeEstudoEngegraphSistemas.Cliente.dll";
        public override string NomeDllInterface => "ProjetoDeEstudo.Utilitarios.dll";
    }

    public interface XIPontoDeInicializacao
    {
    }

    public class XConfig : BaseDeConfig<XConfig>
    {
        public XConfig(): base("SelosApredizado",XTipoDeCriptografia.Aes)
        {
            
        }
        public static CultureInfo Cultura = CultureInfo.GetCultureInfo("pt-BR");
        public String IpServidor
        {
            get { return Get(() => IpServidor); }
        }
    }
}
