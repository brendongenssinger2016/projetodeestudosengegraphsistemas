﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.IoC;

using ProjetoEstudos.Servidor.Interfaces;

namespace ProjetoDeEstudoEngegraphSistemas.Cliente
{
    public class Program
    {static void ExecutarProcesso()
        {
            using (var servidor = XKernel.Get<XISelos>())
            {
                var _senha = "DevEngegraph";
                var usuario = "Engegraph";
                servidor.Login(usuario, _senha);
            }
        }
    }
}
