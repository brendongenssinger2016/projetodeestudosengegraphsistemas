﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.IoC;
using Engegraph.Comum.Utilitarios.Servico;
using ProjetoDeEstudoEngegraphSistemas.Cliente.Inicializador;

namespace ProjetoDeEstudoEngegraphSistemas.Cliente.Servidores
{
    public class XSelos : XServidor
    {
        public XSelos() : base(XKernel.Get<XConfig>().IpServidor,XTipoDeRegistroNoServidor.TcpIp,7409)
        {
            
        }
    }
}
