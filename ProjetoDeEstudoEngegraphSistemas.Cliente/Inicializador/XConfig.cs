﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.Configuracao;


namespace ProjetoDeEstudoEngegraphSistemas.Cliente.Inicializador
{
    public class XConfig : Engegraph.Comum.Utilitarios.Configuracao.BaseDeConfig<XConfig>
    {
        public XConfig() : base("DesenvolvendoSeloEs",XTipoDeCriptografia.Aes)
        {
            
        }

        public static CultureInfo Cultura = CultureInfo.GetCultureInfo("pt-BR");

        public String IpServidor
        {
            get { return Get(() => IpServidor); }
        }
    }
}
