﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using ProjetoEstudos.Servidor.Servidores;

namespace ProjetoEstudos.Servidor.IOC
{
    public class XModuloServidor : NinjectModule
    {
        public override void Load()
        {
            Bind<XServidorTCP>().ToSelf().InSingletonScope();
            Bind<XServidorWsdl>().ToSelf().InSingletonScope();
            Bind<XServidorAtualizacao>().ToSelf().InSingletonScope();
        }
    }
}
