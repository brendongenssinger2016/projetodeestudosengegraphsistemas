﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.CompatibilidadeSql;
using Engegraph.Comum.Utilitarios.Interfaces;
using Ninject.Modules;
using ProjetoEstudos.Servidor.Auxiliares;

namespace ProjetoEstudos.Servidor.IOC
{
    public class XModuloDeInfraEstrutura : NinjectModule
    {
        public override void Load()
        {
            Bind<IDbCommand, IEngCommand, EngCommand>().To<EngCommand>().InTransientScope();
            Bind<DbCommand>().ToMethod(pX => XFactory.NovoComandoGenerico()).InTransientScope();
        }
    }
}
