﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios;
using Engegraph.Comum.Utilitarios.Interfaces;
using Engegraph.Comum.Utilitarios.IoC;
using Engegraph.Comum.Utilitarios.Seguranca;
using Engegraph.Comum.Utilitarios.Sessao;
using ProjetoDeEstudo.Utilitarios.Integracao;
using ProjetoEstudos.Servidor.Interfaces;

namespace ProjetoEstudos.Servidor
{
    public class XSeloServico : XISelos
    {
        public void Dispose()
        {
        }

        public XUsuario Login(string pLogin, string pSenha)
        {
            var cmd = XKernel.Get<IEngCommand>();
            try
            {
                pLogin = pLogin.ToUpper().Trim();
                cmd.CommandText = @"SELECT USU.CHAVE_USUARIO, 
                                            USU.CHAVE_PERFIL,
                                            USU.NOME, 
                                            USU.EMAIL,
                                            USU.LOGIN, 
                                            USU.SENHA,   
                                            USU.MASTER,
                                            USU.CARGO
                                     FROM USUARIOS USU 
                                     WHERE USU.LOGIN = @LOGIN ";

                cmd.AddParameterWithValue("LOGIN", pLogin);

                XUsuario usuarioRetorno;
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var senhaBanco = CriptografiaEng.Descriptografar(reader["SENHA"].AsString());
                        if (senhaBanco == pSenha)
                        {
                            var usuario = new XUsuario
                            {
                                ChaveUsuario = reader["CHAVE_USUARIO"].AsInt32(),
                                ChavePerfil = reader["CHAVE_PERFIL"].AsInt32(),
                                Nome = reader["NOME"].AsString(),
                                Email = reader["EMAIL"].AsString(),
                                Login = reader["LOGIN"].AsString(),
                                Master = reader["MASTER"].BooleanDoObjeto(),
                                Cargo = reader["CARGO"].AsString()
                            };
                            usuarioRetorno = usuario;
                        }
                        else
                            throw new FaultException("Senha Invalida!");
                    }
                    else
                        throw new FaultException("Usuário não encontrado");

                }

                return usuarioRetorno;
            }
            catch (Exception e)
            {
                cmd.Transaction?.Rollback();
                if (e is FaultException)
                    throw;
                throw new FaultException("Erro Desconhecido: " + e.Message);
            }
            finally
            {
                cmd.Liberar();
            }

        }

    
    }
}
