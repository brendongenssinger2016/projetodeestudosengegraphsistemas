﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engegraph.Comum.Utilitarios.Servico;

namespace ProjetoEstudos.Servidor.Servidores
{
    public class XServidorWsdl : XServidor
    {
        public XServidorWsdl(): base("0.0.0.0", XTipoDeRegistroNoServidor.WSDL, 7408) { }
    }
}
