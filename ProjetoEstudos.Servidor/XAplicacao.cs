﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Runtime.Remoting.Messaging;
using Engegraph.Comum.Utilitarios;
using Engegraph.Comum.Utilitarios.Agendamentos;
using Engegraph.Comum.Utilitarios.IoC;
using Ninject;
using ProjetoEstudos.Servidor.Interfaces;
using ProjetoEstudos.Servidor.IOC;
using ProjetoEstudos.Servidor.Servidores;

namespace ProjetoEstudos.Servidor
{
    public static class XAplicacao
    {
        private static readonly List<XBaseDeAcao> Acoes = new List<XBaseDeAcao>();
        
        public static void Iniciar(Boolean pEhConsole)
        {
            XKernel.Instancia = new StandardKernel(new XModuloServidor());
            ConnectarComOBancoDeDados();
            RegistrarInterfaces();
        }

        private static void ConnectarComOBancoDeDados()
        {
            Console.WriteLine("Verificando conexão com o banco de dados.");
            var cmd = XKernel.Get<DbCommand>();
            cmd.Liberar();
        }

        private static void RegistrarInterfaces()
        {
            try
            {
                Console.WriteLine("Iniciando Servidor");
                XKernel.Get<XServidorTCP>().RegistarServidor<XISelos,XSeloServico>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}