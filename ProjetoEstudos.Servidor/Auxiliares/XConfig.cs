﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Engegraph.Comum.Utilitarios;
using Engegraph.Comum.Utilitarios.Configuracao;
using Engegraph.Comum.Utilitarios.PropertyChanged;
using Engegraph.Comum.Utilitarios.Seguranca;

namespace ProjetoEstudos.Servidor.Auxiliares
{
    public class XConfig : XAppConfigBasico<XConfig>
    {
        public CultureInfo Cultura = CultureInfo.GetCultureInfo("pt-BR");

        private static string _conexaoBanco;
        private static string _usuarioBanco;
        private static string _senhaBanco;
        private static XTipoDeConexao? _tipoDeConexao;
        private static string _token;
        private static string _codigoCartorio;
        private static string _chaveSegurancaCartorio;
        private static string _hashDoCartorio;
        private static string _login;
        private static string _senha;
        private static string _webserviceTj;

        public static string ConexaoBanco
        {
            get { return _conexaoBanco ?? (_conexaoBanco = CriptografiaEng.Descriptografar(Get(() => ConexaoBanco))); }
        }

        public static string UsuarioBanco
        {
            get { return _usuarioBanco ?? (_usuarioBanco = CriptografiaEng.Descriptografar(Get(() => UsuarioBanco))); }
        }

        public static string SenhaBanco
        {
            get { return _senhaBanco ?? (_senhaBanco = CriptografiaEng.Descriptografar(Get(() => SenhaBanco))); }
        }

        public static string Token
        {
            get { return _token ?? (_token = CriptografiaEng.Descriptografar(Get(() => Token))); }
        }

        public static string CodigoCartorio
        {
            get { return _codigoCartorio ?? (_codigoCartorio = CriptografiaEng.Descriptografar(Get(() => CodigoCartorio))); }
        }

        public static string ChaveSegurancaCartorio
        {
            get { return _chaveSegurancaCartorio ?? (_chaveSegurancaCartorio = CriptografiaEng.Descriptografar(Get(() => ChaveSegurancaCartorio))); }
        }

        public static string HashDoCartorio
        {
            get { return _hashDoCartorio ?? (_hashDoCartorio = CriptografiaEng.Descriptografar(Get(() => HashDoCartorio))); }
        }

        public static string Login
        {
            get { return _login ?? (_login = CriptografiaEng.Descriptografar(Get(() => Login))); }
        }

        public static string Senha
        {
            get { return _senha ?? (_senha = CriptografiaEng.Descriptografar(Get(() => Senha))); }
        }

        public static string WebserviceTj
        {
            get { return _webserviceTj ?? (_webserviceTj = CriptografiaEng.Descriptografar(Get(() => WebserviceTj))); }
        }

        public static XTipoDeConexao TipoDeConexao
        {
            get
            {
                if (_tipoDeConexao.HasValue)
                    return _tipoDeConexao.Value;
                _tipoDeConexao = (XTipoDeConexao)CriptografiaEng.Descriptografar(Get(() => TipoDeConexao)).AsInt32();
                return _tipoDeConexao.Value;
            }
        }

        public static string Get<TProperty>(Expression<Func<TProperty>> pProperty)
        {
            return Get(pProperty.GetMemberInfo().Name);
        }

        private static string Get(string pKey)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(typeof(XConfig).Assembly.Location);

            KeyValueConfigurationElement element = config.AppSettings.Settings[pKey];
            if (element != null)
            {
                string value = element.Value;
                if (!string.IsNullOrEmpty(value))
                    return value;
            }

            return string.Empty;
        }
    }
}
