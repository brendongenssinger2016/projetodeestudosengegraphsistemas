﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Engegraph.Comum.Utilitarios;
using FirebirdSql.Data.FirebirdClient;

namespace ProjetoEstudos.Servidor.Auxiliares
{
    public static class XFactory
    {
        public static DbCommand NovoComandoGenerico()
        {
            return (DbCommand) NovoComandoFb();
        }
        private static DbCommand NovoComandoSqlServer()
        {
            throw new NotImplementedException();
        }

        private static DbCommand NovoComandoFb()
        {
            try
            {
                var stringCon = string.Format(
                    @"User={2}; Password={3}; Database={0}; DataSource={1}; Port=3051; Dialect=3; Charset=ISO8859_1; Role=; Connection lifetime=15; Pooling=true; MinPoolSize=0; MaxPoolSize=50; Packet Size=8192; ServerType=0",
                    @"localhost:C:\Desenvolvimento\Execucao\BD\SELOS_ES.GDB",
                    "127.0.0.1",
                    "SYSDBA", "masterkey");
                var cmd = new FbCommand("", new FbConnection(stringCon));
                cmd.Connection.Open();
                return cmd;
            }
            catch (Exception ex)
            {
                throw new Exception($@"Não foi possivel conectar na base de dados. Tentando novamente em 15 segundos.
                Dados de conexão - Tipo: Firebird
                ConexaoBanco: {XConfig.ConexaoBanco}
                Usuario: {XConfig.UsuarioBanco}.
                Erro: {ex.Message}
                -------------------------------------------------------------------------------
                ");
            }
        }
    }
}
